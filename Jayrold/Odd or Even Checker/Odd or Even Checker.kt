import java.util.*
import kotlin.system.exitProcess

fun main(){

    try {
        println("Anong number ang gusto mong icheck natin")
        val num1: Int = readLine()!!.toInt()

        if (num1 % 2 == 0)
            println("Even yan pre\n")
        else
            println("Odd pre\n")

        tanong()
    }catch (e: NumberFormatException){
        println("Hindi naman number nilagay mo\n")
        main()
    }

}

fun tanong(){
    println("May tanong ka pa ulet?")
    println("1 kung oo, 2 kung wala")
    val sagot = readLine()

    when (sagot){
        "1" ->{
            main()
        }
        "2" ->{
            println("\ngege")
        System.exit(0)
        }
        else ->{
            println("Ilagay ang tamang numero")
            tanong()
        }
    }
}