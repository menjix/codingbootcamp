

// Write a Java program to compute body mass index (BMI).
fun main(){

    println("\nBMI Calculator!")
    println("\nPlease provide the following")
    bmi()
}


fun bmi(){



    println("Your weight in kilograms: ")
    var kilo = readLine()!!.toDouble()
    println("Your Height in Centimeters")
    var height = readLine()!!.toDouble()

    var centiTometer = height.div(100)
    var metersqrd = centiTometer * centiTometer
    var finalresult = kilo / metersqrd

    println("Your BMI: $finalresult")
    tryagain()

}

fun tryagain(){
    println("Do you want to try again? Y | N")
    var ta = readLine()

    when (ta){

        "Y", "y" -> {
            println("Please, re-enter the following")
            bmi()
        }
        "N", "n" -> {
            println("Thank you")
            System.exit(0)
        }
        else ->{
            println("Please enter the right letter of your choice")
            tryagain()
        }
    }
}



// reference: https://www.wikihow.com/Calculate-Your-Body-Mass-Index-(BMI)