
// Write a Java program to takes the user for a distance (in meters) and the time was taken
// (as three numbers: hours, minutes, seconds), and display the speed, in meters per second,
// kilometers per hour and miles per hour (hint: 1 mile = 1609 meters)

fun main(){

    println("Physics Calculator!")
    println("Print enter the needed details\n")
    physics()
}


fun physics(){

    println("Distance in meters: ")
    var meter = readLine()!!.toDouble()
    println("Time taken in hours: ")
    var hours = readLine()!!.toDouble()
    println("Time taken in minutes: ")
    var minute = readLine()!!.toDouble()
    println("Time taken in seconds: ")
    var seconds = readLine()!!.toDouble()

    var totalsec = (hours * 3600) + (minute * 60) + seconds
    var resultms = meter.div(totalsec)
    var resultkmh = (meter * 3600) / (totalsec * 1000)
    var resultmh = (meter * 3600 * 1) / (totalsec * 1000 * 1.609)

    println("Your speed in meters/second is: $resultms")
    println("Your speed in km/h is: $resultkmh ")
    println("Your speed in miles/h: $resultmh")
    tryagain()
}

fun tryagain(){
    println("Do you want to try again? Y | N")
    var ta = readLine()

    when (ta){

        "Y", "y" -> {
            println("Please, re-enter the following")
            physics()
        }
        "N", "n" -> {
            println("Thank you")
            System.exit(0)
        }
        else ->{
            println("Please enter the right letter of your choice")
            tryagain()
        }
    }
}



}

//reference: http://www.mathsmutt.co.uk/files/dst.htm