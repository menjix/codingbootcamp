import kotlin.system.exitProcess

fun main() {

    characters()
    println("\n Kumuhang muli ng tala?")
    ulit()

}

fun characters() {
    println("\n")
    println("Ibil Contact Tracing\n")
    print("Petsa:  ")
    val petsa = readLine()
    print("Oras ng Pagsamba:  ")
    val op = readLine()
    print("Pangalan:  ")
    val pangalan = readLine()
    print("Edad:  ")
    val edad = readLine()
    print("Address:  ")
    val addr = readLine()
    print("PRK-GRP:  ")
    val pg = readLine()
    print("Tungkulin:  ")
    val mt = readLine()
    print("Contact Number:  ")
    val cn = readLine()
    print("Email Address:  ")
    val ea = readLine()

    println("\n* PUNAN NG MAGSUSURI *\n")
    print("Temperatura:  ")
    val temp = readLine()
    print("\nLagyan ng (x) kung negatibo sa mga sumusunod na sintomas\n")
    print("Naobserbahan na may pag-uubo o nahihirapang huminga:  ")
    val ubo = readLine()
    print("Naobserbahan na may pag-tatae o pananakit ng tiyan:  ")
    val tae = readLine()
    print("Naobserbahan na may pananakit ng katawan o kasukasuan:  ")
    val saket = readLine()
    print("Pangalan ng sumuri:  "  )
    val mtc = readLine()

    print("\n")
    print("----------------- Contact Tracing -----------------\n")
    print("- Petsa: $petsa\n")
    print("- Oras ng Pagsamba: $op\n")
    print("- Pangalan: $pangalan\n")
    print("- Edad: $edad\n")
    print("- Address: $addr\n")
    print("- Prk-Grp: $pg\n")
    print("- Tungkulin: $mt\n")
    println("- Contact Number: $cn")
    println("- Email Address: $ea")
    println("----------------------------------------------------\n")
    println("Temperatura: $temp")
    println("Naobserbahan na may pag-uubo o nahihirapang huminga: $ubo ")
    println("Naobserbahan na may pag-tatae o pananakit ng tiyan: $tae ")
    println("Naobserbahan na may pananakit ng katawan o kasukasuan: $saket ")
    println("Pangalan ng sumuri: $mtc ")

    println("\n Note: Ang lagnat ay temperatura na 37.7C o higit pa")

}

fun ulit(){

    println(" Y kung oo, X kung hindi na")
    val kmt = readLine()
    when(kmt){
        "Y", "y" ->{
            main()
        }
        "X", "x" ->{
            println("Maraming salamat sa inyong pagtugon")
            exitProcess(0)
        }
        else ->{
            println("Piliin ang tamang letra ng tugon")
            ulit()
        }
    }
}