package com.jayrold.revision
import java.util.Scanner
val reader = Scanner(System.`in`)

fun main() {
    numericalInputs()
    tanong()
}

fun numericalInputs() {

    try {
        println("Ilagay ang unang numerong gusto mong kalkulahin")
        var num1: Int = readLine()!!.toInt()
        println("Ilagay ang susunod na numero")
        var num2: Int = readLine()!!.toInt()


        println("---------- Gusto mo $num1 at $num2? ----------\n")

        try {
            println("Pumili ng operator")
            println("1 kung + ")
            println("2 kung -")
            println("3 kung *")
            println("4 kung /")

            var operator = readLine()!!.toInt()
            when (operator) {
                1 -> {
                    var result =  num1 + num2
                    print("$num1 + $num2 = $result\n")
                    tanong()
                }
                2 -> {
                    var result =  num1 - num2
                    print("$num1 + $num2 = $result\n")
                    tanong()
                }
                3 -> {
                    var result =  num1 * num2
                    print("$num1 + $num2 = $result\n")
                    tanong()
                }
                4 -> {
                    var result =  num1 / num2
                    print("$num1 + $num2 = $result\n")
                    tanong()
                }
                else -> {
                    println("Ilagay ang tamang numero ng operator")
                    error()
                }
            }

        } catch (e: NumberFormatException) {
            println("Ilagay ang tamang numero ng operator")
            error()
        }
    } catch (e: NumberFormatException) {
        println("Numbers lang tinatanggap ko")
        error()
    }
}

fun error() {

    try {
        println("Muling ilagay ang unang numerong gusto mong kalkulahin")
        var num1: Int = readLine()!!.toInt()
        println("Muling ilagay ang susunod na numero")
        var num2: Int = readLine()!!.toInt()

    println("---------- Gusto mo $num1 at $num2? ----------\n")

    try {
      println("Pumili ng operator")
      println("1 kung + ")
      println("2 kung -")
      println("3 kung *")
      println("4 kung /")
      var operator = readLine()!!.toInt()
      when (operator) {
          1 -> {
              var result =  num1 + num2
              print("$num1 + $num2 = $result\n")
              tanong()
          }
          2 -> {
              var result =  num1 - num2
              print("$num1 + $num2 = $result\n")
              tanong()
          }
          3 -> {
              var result =  num1 * num2
              print("$num1 + $num2 = $result\n")
              tanong()
          }
          4 -> {
              var result =  num1 / num2
              print("$num1 + $num2 = $result\n")
              tanong()
          }
          else -> {
              println("Ilagay ang tamang numero ng operator")
              error()
          }
      }

   } catch (e: NumberFormatException) {
        println("Ilagay ang tamang numero ng operator")
        error()
    }
} catch (e: NumberFormatException){
        println("Numbers lang tinatanggap ko")
        error()

    }
}

fun tanong() {
    println("\nMay tanong ka pa ulet?")
    println("1 kung oo o 2 kung wala na?")
    var answer: String = reader.next()

    when (answer) {
        "1", "oo" -> {
            error()
        }
        "2", "wala na" -> {
            println("mabuti naman")
            println("Geh bye")
            System.exit(0)
        }

        else -> {
            println("Bahala ka 'di ka marunong sumunod")
        }
    }
}


