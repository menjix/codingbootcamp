fun validNumber(str: String) = try{
        str.toDouble()
        true
    } catch (e: NumberFormatException){
        println("Invalid Input")
        false
    }


fun oddOrEven(){
    var num: String
    do{
        print("\nEnter Number: ")
        num = readLine()!!
    }while(!validNumber(num))

    var intNum = num.toInt()
    println()
    if(intNum%2 == 0) println("$intNum is EVEN")
    else println("$intNum is ODD")
}

fun main(){
    oddOrEven()
}