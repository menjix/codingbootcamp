
fun alphaExeption(word: String): Boolean{
    return word.all {it.isLetter()}
}

fun main() {
    reverseWord()
}

fun reverseWord(){
    do{
        print("Enter single Word: ")
        var word = readLine()!!
        if(!alphaExeption(word)) println("Valid Word Only!!")
        else println("Reverse Word: " + word.reversed())
    }while(!alphaExeption(word))

    print("Do you want to Continue?: Y/N: ")
    var response = readLine()
    if(response == "Y" || response == "y") reverseWord()
}