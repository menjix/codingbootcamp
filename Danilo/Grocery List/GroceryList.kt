
val items = arrayOf("Cereal ", "Grits  ", "Bread  ", "Sugar  ", "Buns   ", "Coffee ", "Soda   ", "Water  ", "Tea    ", "Milk   ", "Lasagna", "Pizza  ", "Donut  ", "Burger ", "Rolls  ","Popcorn","Chips  ", "Pretzel", "Peanuts")
val amount = arrayOf(25,15,35,14,10,12,15,10,20,35,40,85,25,28,30,30,15,15,10)
val stock = arrayOf(114,28,513,1024,103,97,87,699,102,165,154,643,197,298,365,143,502,125,1030)
var cart = mutableListOf<String>()
var receipt = mutableListOf<String>()
var total: Int = 0
var scene: Int = 1
var response: String = ""
var qty: String = ""

fun enterResponse(maxRange:Int){
	do{
		print("\nEnter Response: ")
		response = readLine()!!
	}while(!validInput(response,maxRange))
}

fun enterQuantity(){
	do{
		print("Enter Quantity: ")
		qty = readLine()!!
	}while(!validStock(response, qty))
}

fun numberFormatException(input: String): Boolean{
	try{
		input.toInt()
	}catch(e: NumberFormatException){
		return false
	}
	return true
}

fun validInput(response: String, maxRange:Int): Boolean{
	var isValid:Boolean =  numberFormatException(response)

	if(isValid && (response.toInt() < 1 || response.toInt() > maxRange)) isValid = false

	if(!isValid) println("INVALID INPUT!!")

	return isValid
}

fun validStock(response:String, qty: String): Boolean{
	var outOfStock:Boolean =  numberFormatException(qty)

	if(outOfStock && (qty.toInt() < 1 || qty.toInt() > stock[response.toInt()-1])) outOfStock = false

	if(!outOfStock) println("MAX ITEM STOCK IS ${stock[response.toInt()-1]} !!\n")

	return outOfStock
}

fun groceryMenu(){
	do{
		displayScenes()
	}while(scene != 0)
	println("*EXIT!!*")
}

fun displayScenes(){
	if(scene == 1){

		println("\n  $ WELCOME TO MINI GROCERY $")
		println("\nWhat do you want? ")
		println("[1] Show All List of Items")
		println("[2] Exit!")

		enterResponse(2)
		println()

		if(response == "1"){
			displayItems()
			scene = 2 
		}else scene = 0 

	}else if(scene == 2){
		println("\nWhat do you want? ")
		println("[1] Add to cart?")
		println("[2] View Cart")
		println("[3] Delete Item")
		println("[4] Reset")
		println("[5] Back")
		println("[6] Exit!")

		enterResponse(6)
		println()

		if(response == "1"){
			addTocart()
		}else if(response == "2"){
			if(cart.size != 0) displayReceipt()
			else println("*No Item to View*")
		}else if(response == "3"){
			if(cart.size != 0) deleteItem()
			else println("*No Item to Delete*")
		}else if(response == "4"){
			if(cart.size != 0) resetItem()
			else println("*No Item to Reset*")
		}else if(response == "5"){
			scene = 1
		}else scene = 0 
	}
}

fun displayItems(){
	println("ITEM\t\tPRICE\t\tSTOCK")
	for(i in 0..items.size-1) println(items[i] + "\t\t P" + amount[i] + "\t\t " + stock[i])
}

fun displayReceipt(){
	println("ITEM   \t\tAMOUNT\t\tQTY\t\tTOTAL")
	for(i in 0..receipt.size-1) println(receipt[i])
	println("\nTotal Amount: $total")
}

fun deleteItem(){
	println("CODE\tITEM")
	for(i in 0..cart.size-1) println("[${i+1}]  \t" + cart[i])

	println("\nEnter a code you want to Delete.");
	enterResponse(cart.size)

	println("${cart[response.toInt()-1]} was Removed in the Cart");
	cart.removeAt(response.toInt()-1)
	receipt.removeAt(response.toInt()-1)
}

fun resetItem(){
	println("Item Clear!")
	cart.clear()
}

fun addTocart(){
	println("CODE\tITEM\t\tAMOUNT\t\tSTOCK")
	for(i in 0..items.size-1) println("[${i+1}]  \t" + items[i] + "\t\t P" + amount[i] + "\t\t " + stock[i])

	println("\nEnter a code you want to Add to Cart.");
	enterResponse(items.size)
	enterQuantity()

	var itemAmount: Int = amount[response.toInt()-1]*qty.toInt()
	var itemDetails: String = "${items[response.toInt()-1]}\t\t${amount[response.toInt()-1].toString()}     \t\t$qty\t\t${itemAmount}"

	receipt.add(itemDetails)
	cart.add(items[response.toInt()-1])

	total = total+itemAmount
}

fun main() {
    groceryMenu()
}