
fun printPyramid(height: Int){
    println()
    for(i in 0..height-1){
		for(j in height downTo i){
            print(" ")
        }
        for(k in 0..i){
            print("* ")
        }
        println()
    }
}

fun asteriskPyramid(){
	var n: Int = 0
    try{
        print("\nHeight of Pyramid: ")
        var temp = readLine()!!.toInt()
		if(temp > 1) n = temp
		else {
			println("Minimum Height is 2!!")
			asteriskPyramid()
		}
        printPyramid(n)
    }catch(e: NumberFormatException){
        println("Please enter a valid Height!!")
        asteriskPyramid()
	}
}

fun reset(): Boolean{
	var response: Int = 0
	println("Do you want to Continue? ")
	println("1. Yes")
	println("2. No")
	do{
		try{
			print("Response: ")
			var temp = readLine()!!.toInt()
			if(temp == 1 || temp == 2) response = temp
			else println("Enter Valid Input!!")
		}catch(e: NumberFormatException){
			println("Please enter a valid Response!!")
		}
	}while(response == 0)
	if(response == 1) return true
	else return false
}

fun main() {
	var run: Boolean
	do{
		asteriskPyramid()
		run = reset()
	}while(run)
}