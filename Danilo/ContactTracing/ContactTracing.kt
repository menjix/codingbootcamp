
fun contactTracing(){
    println("\n")
    println("  Fill-up Form!   ")
    print("Petsa: ")
    var petsa = readLine()
    print("Oras ng Pagsamba: ")
    var oras = readLine()
    print("Pangalan: ")
    var pangalan = readLine()
    print("Edad: ")
    var edad = readLine()
    print("Address: ")
    var address = readLine()
    print("Purok-Grupo: ")
    var prkgrp = readLine()
    print("Tungkulin: ")
    var tungkulin = readLine()
    print("Contact Number: ")
    var contactNo = readLine()
    print("Email Address: ")
    var emailAdd = readLine()
    print("Temperature: ")
    var temp = readLine()

    println("\n")
    println("###############################################")
    println("                                               ")
    println("              $ CONTACT TRACING $              ")
    println("                                               ")
    println("  Petsa: $petsa")
    println("  Oras ng Pagsamba: $oras")
    println("  Pangalan: $pangalan")
    println("  Edad: $edad")
    println("  Address: $address")
    println("  Purok-Grupo: $prkgrp")
    println("  Tungkulin: $tungkulin")
    println("  Contact Number: $contactNo")
    println("  Email Address: $emailAdd")
    println("  Temperature: $temp")
    println("                                              ")
    println("##############################################")

    print("\n\nDo you want to continue? Y/N : ")
    var response = readLine()
    if (response == "Y" || response == "y") {
        contactTracing()
    }
}

fun main(args: Array<String>) {
    contactTracing()
}