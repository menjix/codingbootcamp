import java.lang.NumberFormatException

fun main(args: Array<String>) {
    run()
}

fun run(){
    try {
        println("\n\n********** Pyramid of Asterisk **********")
        print("Enter the Height of Pyramid:")
        var height = readLine()!!.toInt()
        println("\n")
        if (height > 1) {
            for (a in 1..height) {
                for (b in height downTo a) {
                    print(" ")
                }
                for (c in 1..a) {
                    print("* ")
                }
                println()
            }
            again()
        }else {
            println("2 line is the required height to build a pyramid!")
        }
    } catch (E : NumberFormatException) {
        println("Input a number only!")
    }
}

fun again(){

   try {
       print("\n\n******************** Try again? ********************\nPress 1 if yes or 2 if No and you want to exit the program:")
       var choice = readLine()!!.toInt()
       if (choice == 1) {
           run()
       } else {
           println("Thank you")
       }
   }catch( E : NumberFormatException){
       println("Enter a number only!")
   }
}

