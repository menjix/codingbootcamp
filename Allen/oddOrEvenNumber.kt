import kotlin.system.exitProcess

fun main(args : Array<String>){
    determine()
}

fun determine() {
    println("************* ODD or EVEN *************")
 try{
    print("Enter a number:")
    val num1 = readLine()!!.toInt()

        if (num1 % 2 == 0) {
            println("\nNumber $num1 is Even Number\n")
        }
        else {
            println("\nNumber $num1 is Odd Number\n")
        }
        again()
    }catch(e:NumberFormatException){
        println("\nPlease enter a number only!")
    }
  }


fun again (){
       println("Do you want to continue? If YES press 1 if No press 2")
       var again = readLine()!!.toInt()
       if (again == 1) {
           determine()
       } else if (again == 2) {
           println("Thank You! :)")
           exit()
       } else {
           println("Please enter number 1 or 2 only! ")
           exit()
       }

}


fun exit(){
  exitProcess(1)
}

