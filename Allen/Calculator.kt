import kotlin.system.exitProcess

fun main (args : Array<String>){
    run()
}
fun run() {
    println("\n\n*************** CALCULATOR ***************")
    println("Choose a number for the desired operation:")
    println("Press 1 for ADDITION\nPress 2 for SUBTRACTION\nPress 3 for MULTIPLICATION\nPRESS 4 for DIVISION\nPRESS 5 to EXIT")
    print("\nEnter your choice:")
    try {
        var choice: Int = readLine()!!.toInt()

        when (choice) {
            1 -> add()
            2 -> sub()
            3 -> mul()
            4 -> div()
            5 -> exit()
        }
    }catch(e : NumberFormatException){
        println("\n!!Please enter a Number only!!\n")
        again()
    }

}




fun add(){
        println("\n************* YOU CHOOSE ADDITION ***************")

        print("\nPlease enter the first number you want to add:")

        val num1 = readLine()!!.toInt()

        print("\nPlease enter the second number you want to add:")

        val num2 = readLine()!!.toInt()

        val sum: Int = num1 + num2

        println("\n***** You enter $num1 + $num2 *****\n The sum is: $sum \n\n")

        again()
}

fun sub(){
    println("\n************* YOU CHOOSE SUBTRACTION ***************")
    print("\nPlease enter the first number you want to subtract:")
    val num1 = readLine()!!.toInt()
    print("\nPlease enter the second number you want to subtract:")
    val num2 = readLine()!!.toInt()

    val diff: Int = num1 - num2


    println("\n***** You enter $num1 - $num2 *****\n The difference is: $diff\n\n")

    again()
}
fun mul (){
    println("\n************* YOU CHOOSE MULTIPLICATION ***************")
    print("\nPlease enter the first number you want to multiply:")
    val num1 = readLine()!!.toInt()
    print("\nPlease enter the second number you want to multiply:")
    val num2 = readLine()!!.toInt()

    val product: Int = num1 * num2


    println("\n***** You enter $num1 * $num2 *****\n The product is: $product\n\n")

    again()
}



fun div(){
    println("\n************* YOU CHOOSE DIVISION ***************")
    print("\nPlease enter the first number you want to divide:")
    val num1 = readLine()!!.toInt()
    print("\nPlease enter the second number you want to divide:")
    val num2 = readLine()!!.toInt()

    val div: Int = num1 / num2


    println("\n***** You enter $num1 * $num2 *****\n The quotient is: $div\n\n")

    again()
}

fun again(){
    print("DO YOU WANT TO TRY AGAIN?\n\nPRESS 1 IF YES 2 FOR NO:")
    try {
        var choice = readLine()!!.toInt()

        when (choice) {
            1 -> run()
            2 -> exit()
            else -> {
                println("\nThe number you choose is not in option\n\nPress 1 for yes and 2 for No\n")
            again()
            }
        }

    }catch (e:NumberFormatException){
        println("\n!!Please enter a number only!!\n")
        again()
    }

}

fun exit(){
    println("\nThank you for using my CALCULATOR :)")
    exitProcess(1)
}

