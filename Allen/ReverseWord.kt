import java.lang.NumberFormatException

fun main (args : Array<String>){
run()
}

fun run(){

    println("\n*************** String Reversal ***************")
    println("\nEnter the word you want to reverse:")
    var word = readLine().toString()
    var wordlength = word.length
    var result = ""
    for (a in wordlength-1 downTo 0){
        var reverseword = word[a]
        result += reverseword
    }
    println("\n\nThe reverse word of $word is: \n$result\n\n")
    again()
}

fun again(){

    try {
        println("Want to input another word to reverse?\nIf yes press 1 if no press 2:")
        var choice = readLine()!!.toInt()

        if (choice == 1) {
            run()
        } else {
            println("\nThank you!")
        }
    }catch (E:NumberFormatException){
        println("Enter a Number only!")
    }

}
