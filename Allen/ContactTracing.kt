
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.system.exitProcess

fun main (args: Array<String>){

    run()

}




fun run(){
    try {
        println("*********** CONTACT TRACING **********")
        println("Enter your name:")
        var name = readLine().toString()
        println("Enter your age:")
        var age = readLine()!!.toInt()
        println("Enter your Contact Number:")
        var contactNo = readLine()!!.toLong()
        println("Enter your home address:")
        var homeadd = readLine().toString()
        println("Enter your email address:")
        var email = readLine().toString()
        val date = LocalDateTime.now()
        println("Enter your temperature")
        val temp = readLine()!!.toFloat()

        println("NAME:$name")
        println("AGE:$age")
        println("CONTACT NO.:$contactNo")
        println("HOME ADDRESS:$homeadd")
        println("Email:$email")
        println("DATE:${date.format(DateTimeFormatter.ISO_DATE)}")
        println("TEMPERATURE:$temp")
        again()
    }catch(e:IOException){

    }

}

fun again(){
    println("Do you want to try again? Press 1 if YES and Press 2 if NO:")
    var choice = readLine()!!.toInt()
    when(choice) {
        1 -> run()
        2 -> println("Thank You!")

        else -> println("Please enter 1 or 2 only!")
    }
}